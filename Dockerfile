FROM golang:latest as builder

COPY . .
RUN CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o /helloworld main.go

FROM scratch
COPY --from=builder /helloworld /helloworld
ENTRYPOINT [ "/helloworld" ]

