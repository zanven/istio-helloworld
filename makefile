
.PHONY: build
build:
	docker build -t zanven42/helloworld:latest .
.PHONY: deploy
deploy:
	# make sure your istio can sidecar inject by default
	# https://istio.io/docs/setup/kubernetes/sidecar-injection/
	# kubectl edit configmaps istio-sidecar-injector --namespace istio-system
	#
	kubectl apply -f namespace.yaml -f gateway.yaml -f deployment.yaml -f virtualservice.yaml -f service.yaml
